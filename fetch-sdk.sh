#!/bin/bash
set -e
set -u

SDK_NAME=$SDK_NAME # From the Dockerfile environment
TOOLCHAIN_URL="https://cloudpebble-vagrant.s3.amazonaws.com/arm-cs-tools-stripped.tar"
TOOLCHAIN_FILE=$(basename $TOOLCHAIN_URL)
SDK_URL="http://assets.getpebble.com.s3-website-us-east-1.amazonaws.com/sdk2/${SDK_NAME}.tar.gz"
SDK_FILE=$(basename $SDK_URL)
SDK_DIR=$SDK_NAME

function install_sdk {
    echo -e "\nInstalling SDK"
    mkdir -p $HOME/pebble-dev
    cd $HOME/pebble-dev

    if [ ! -e $HOME/$SDK_FILE ]; then
        rm -f $SDK_FILE
        echo "- Fetching SDK"
        curl -# -o $SDK_FILE $SDK_URL
    else
        mv $HOME/$SDK_FILE .
    fi
    echo "- Unpacking SDK"
    tar xf $SDK_FILE

    if [ ! -e $HOME/$TOOLCHAIN_FILE ]; then
        rm -f $TOOLCHAIN_FILE
        echo "- Fetching toolchain"
        curl -# -o $TOOLCHAIN_FILE $TOOLCHAIN_URL
    else
        mv $HOME/$TOOLCHAIN_FILE .
    fi
    cd $SDK_DIR
    echo "- Unpacking toolchain"
    tar xf ../$TOOLCHAIN_FILE

    rm -rf .env
    echo "- Setting up virtualenv"
    virtualenv -q .env
    set +u # Otherwise the source fails
    source .env/bin/activate
    set -u
    echo "- Installing requirements"
    pip install -r requirements.txt > virtualenv.log

    touch $HOME/.sdk_ready
    echo "Done."
}

if [ ! -e $HOME/.tos_accepted ]; then
    echo -e "\nHave you read and accepted the Pebble Developer ToS\nat https://auth.getpebble.com/terms_of_use?\n"
    read -r -p "[y/N] " res
    case $res in
        [yY][eE][sS]|[yY])
            touch $HOME/.tos_accepted
            install_sdk
            ;;
        *)
            exit 1
            ;;
    esac
elif [ ! -e $HOME/.sdk_ready ]; then
    install_sdk
fi
