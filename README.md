Pebble-Dev
==========

An Ubuntu 12.04 based Pebble smartwatch development environment.

SDK Redistribution
==================

As the SDK cannot be redistributed in the auto-built docker container it is
fetched when the image is first run and the ToS has been accepted.

The resulting complete container can then be commited into a new image.

Example:
 - ```docker run -ti --name with-sdk swid/pebble-dev```
 - Accept the ToS and wait for the SDK to install
 - (Optional: ```echo export PEBBLE_PHONE=xxx.yyy.zzz.aaa >> ~/.bashrc```)
 - ```docker commit with-sdk pebble-dev```
 - ```docker rm with-sdk```

Now you can use the ```pebble-dev``` image as intended.

Usage
=====

By default an interactive bash for the user 'pebble' is spawned.

```docker run -ti --name my-pebble-project pebble-dev```

Tell the SDK where to find your phone if you didn't do it during setup
```export PEBBLE_PHONE=192.168.x.y```

The SDK bin directory is in the PATH, so you can just use the ```pebble``` command

To re-start a previous session run:

```docker start -ai my-pebble-project```

Ping your Pebble
================

Ping the pebble
```pebble ping```

That should be it

TODO
====
  - Find out how to use a direct bluetooth connection (Forward the ```/dev/rfcomm``` device to the container?)
